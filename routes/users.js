//[SECTION] Dependencies and Modules
const exp = require("express"); 
const controller = require('./../controllers/users.js');
const auth = require("../auth") 

// destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

// [SECTION] Routes-[POST]
route.post('/register', (req, res) => {
  let userDetails = req.body; 
  controller.registerUser(userDetails).then(outcome => {
     res.send({message: "outcome"});
    // return true;
  });
});

// route.post("/register", controller.registerUser);

// LOGIN

route.post("/login", controller.loginUser);

// GET USER DETAILS
//[SECTION] Routes-[GET]

// note: routes that have verify as a middleware would require us to pass a token from postman
route.get("/getUserDetails", verify, controller.getUserDetails)


//Enroll our registered users
route.post('/enroll', verify, controller.enroll)


//get logged user's enrollments
route.get('/getEnrollments', verify, controller.getEnrollments)




















//[SECTION] Routes-[PUT]
//[SECTION] Routes-[DELETE]

//[SECTION] Expose Route System
module.exports = route; 
