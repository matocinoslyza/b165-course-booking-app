// [SECTION] Dependencies and Modules
const Course = require('../models/Course');

// [SECTION] Functionality [CREATE]
module.exports.createCourse = (info) => {
    let cName = info.name;
    let cDesc = info.description;
    let cCost = info.price;

    let newCourse = new Course({
        name: cName,
        description: cDesc,
        price:  cCost
    })

    return newCourse.save().then((savedCourse, error) => {
        if (error) {
            return false;
        }else{
    
            return savedCourse;
        }
    })
}
// [SECTION] Functionality [RETRIEVE]
    module.exports.getAllCourse = () => {
        return Course.find({}).then(result => {
            return result;
        });
    };
    // Retrieve a single course 
    module.exports.getCourse = (id) => {
            return Course.findById(id).then(resultOfQuery => {
                return resultOfQuery;
            });
    };
    module.exports.getAllActiveCourse = () => {
        return Course.find({isActive: true}).then(resultOftheQuery => {
            return resultOftheQuery;
        });
    };

// [SECTION] Functionality [UPDATE]
    module.exports.updateCourse  = (id, details) => {
        let cName = details.name;
        let cDesc = details.description;
        let cCost = details.price;

        let updatedCourse = {
            name: cName, 
            description: cDesc,
            price: cCost
        }

        return Course.findByIdAndUpdate(id, details).then((courseUpdated, err) => {
            if (err) {
                return false;
            }else{
                return true;
            }
        }); 
    };
// Deactivate Course
    module.exports.deactivateCourse = (id) => {
        let updates = {
            isActive: false
        }
        return Course.findByIdAndUpdate(id, updates).then((archived, error) => {
            if (archived) {
                return true;
            }else{
                return false;
            };
        });
    };
// Reactivate Course
    module.exports.reactivateCourse = (id) => {
        let updates = {
            isActive: true
        }
        return Course.findByIdAndUpdate(id, updates).then((reactivate, error) => {
            if (reactivate) {
                return false;
            }else{
                return true;
            };
        });
    };

    // [SECTION] Functionality [DELETE]
    module.exports.deleteCourse = (id) => {
        return Course.findByIdAndRemove(id).then((removeCourse, err) => {
        if (err) {
            return 'No Course Was Removed';
        }else{
            return 'Course Sucessfully Deleted'
        }
        });

    };

 