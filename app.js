// [SECTION] Packanges and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const courseRoutes = require('./routes/courses');
const userRoutes = require('./routes/users');

const cors = require("cors");

// [SECTION] Server Setup
const app = express();
dotenv.config();
app.use(express.json());
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT || 4000;

app.use(cors());


// [SECTION] Application Routes
    app.use('/courses', courseRoutes);
    app.use('/users', userRoutes);

// [SECTION] Database Connect
mongoose.connect(secret)
let connectionStatus = mongoose.connection;
connectionStatus.on('open', () => console.log('Database is connected'));

// [SECTION] Gateway Response
app.get('/', (req, res) => {
    res.send(`Welcome to Lyza's Course Booking App!`);
});

app.listen(port, () => console.log(`Server is running on port ${port}`));


